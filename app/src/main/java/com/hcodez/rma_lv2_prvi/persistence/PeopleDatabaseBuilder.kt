package com.hcodez.rma_lv2_prvi.persistence

import androidx.room.Room
import com.hcodez.rma_lv2_prvi.Rma_lv2_prvi

object PeopleDatabaseBuilder {
    private var instance: PeopleDatabase? = null

    fun getInstance(): PeopleDatabase {
        synchronized(PeopleDatabase::class) {
            if (instance == null) {
                instance = buildDatabase()
            }
        }
        return instance!!
    }

    private fun buildDatabase(): PeopleDatabase {
        return Room.databaseBuilder(
            Rma_lv2_prvi.application, PeopleDatabase::class.java, PeopleDatabase.NAME
        )
            .allowMainThreadQueries()
            .build()
    }
}