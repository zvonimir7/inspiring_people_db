package com.hcodez.rma_lv2_prvi.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "people")
data class InspiringPerson(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "birthDate") val birthDate: String,
    @ColumnInfo(name = "description") val description: String
) : Serializable