package com.hcodez.rma_lv2_prvi.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hcodez.rma_lv2_prvi.R
import com.hcodez.rma_lv2_prvi.models.InspiringPerson

class InspiringPersonAdapter(
    scientists: List<InspiringPerson>
) :
    RecyclerView.Adapter<PersonViewHolder>() {
    private val scientists: MutableList<InspiringPerson> = mutableListOf()

    init {
        refreshData(scientists)
    }

    fun refreshData(scientists: List<InspiringPerson>) {
        this.scientists.clear()
        this.scientists.addAll(scientists)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.inspiring_person_item, parent, false)
        return PersonViewHolder(view, parent.context)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val scientist = scientists[position]
        holder.bind(scientist)
        holder.itemView.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return scientists.size
    }
}